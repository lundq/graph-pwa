import Vue from 'vue'
import Vuex from 'vuex'
import { INSERT_GRAPH,
  SET_NODE_PAGE_NUMBER,
  SET_ROOT_NODE, SET_SELECTED_NODE,
  SET_NAV_CURSOR, PUSH_NAV_HISTORY,
  SET_SEARCH_IN_FOCUS,
  ADD_NODE, ADD_EDGE
} from './mutation-types'

Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    modules: {
    },
    state: {
      config: {
        numberOfCardsPerPage: 3,
        retrieveAllChildNodeCards: true,
        cardLabels: {
          Participant: {
            id: 'id',
            name: 'name',
            label: 'label',
            description: 'content',
            link: 'webpage'
          }
        },
        tileLabels: {
          ParticipantCategory: {
            id: 'id',
            name: 'name',
            label: 'label'
          },
          Event: {
            id: 'id',
            name: 'name',
            label: 'label'
          }
        },
        rootNodeId: '1'
      },
      rootNode: Object.create(null),
      selectedNode: Object.create(null),
      navData: {
        navHistory: [],
        navCursor: 0,
        nodePageNumbers: new Map()
      },
      searchInFocus: false,
      searchData: {
        // Property name that will be used by filter() to filter the array of objects below.
        field: 'value',

        list: [
          /* {
            value: '', // The value given, when selected
            label: '', // The value displayed as main label for this suggested selection

            sublabel: '', // optional
            icon: '', // optional
            stamp: '' // optional
          } */
        ]
      },
      graph: {
        nodesIndex: Object.create(null),
        nodesArray: [],

        edgesIndex: Object.create(null),
        edgesArray: [],

        inNeighborsIndex: Object.create(null),
        outNeighborsIndex: Object.create(null),
        allNeighborsIndex: Object.create(null),

        inNeighborsCount: Object.create(null),
        outNeighborsCount: Object.create(null),
        allNeighborsCount: Object.create(null)
      }
    },
    mutations: {
      [INSERT_GRAPH] (state, payload) {
        state.graph = payload
      },
      [SET_ROOT_NODE] (state, nodeId) {
        state.rootNode = state.graph.nodesIndex[nodeId]
        state.selectedNode = state.graph.nodesIndex[nodeId]
        state.navData.navHistory.push(nodeId)
        state.navData.navCursor = state.navData.navHistory.indexOf(nodeId)
      },
      [SET_SELECTED_NODE] (state, nodeId) {
        state.selectedNode = state.graph.nodesIndex[nodeId]
      },
      [SET_SEARCH_IN_FOCUS] (state, bool) {
        state.searchInFocus = bool
      },
      [ADD_NODE] (state, node) {
        // Add empty containers for edge indexes:
        state.graph.inNeighborsIndex[node.id] = Object.create(null)
        state.graph.outNeighborsIndex[node.id] = Object.create(null)
        state.graph.allNeighborsIndex[node.id] = Object.create(null)

        state.graph.inNeighborsCount[node.id] = 0
        state.graph.outNeighborsCount[node.id] = 0
        state.graph.allNeighborsCount[node.id] = 0

        state.graph.nodesArray.push(node)
        state.graph.nodesIndex[node.id] = node

        // Add to search data list
        state.searchData.list.push({
          value: node.name, // The value given, when selected
          label: node.name, // The value displayed as main label for this suggested selection

          sublabel: '', // optional
          icon: '', // optional
          stamp: '', // optional
          id: node.id
        })
      },
      [ADD_EDGE] (state, edge) {
        // Add the edge to the indexes:
        state.graph.edgesArray.push(edge)
        state.graph.edgesIndex[edge.id] = edge

        if (!state.graph.inNeighborsIndex[edge.target][edge.source]) {
          state.graph.inNeighborsIndex[edge.target][edge.source] = Object.create(null)
          state.graph.inNeighborsIndex[edge.target][edge.source][edge.id] = edge
        }
        if (!state.graph.outNeighborsIndex[edge.source][edge.target]) {
          state.graph.outNeighborsIndex[edge.source][edge.target] = Object.create(null)
          state.graph.outNeighborsIndex[edge.source][edge.target][edge.id] = edge
        }
        if (!state.graph.allNeighborsIndex[edge.source][edge.target]) {
          state.graph.allNeighborsIndex[edge.source][edge.target] = Object.create(null)
          state.graph.allNeighborsIndex[edge.source][edge.target][edge.id] = edge
        }
        if (edge.target !== edge.source) {
          if (!state.graph.allNeighborsIndex[edge.target][edge.source]) {
            state.graph.allNeighborsIndex[edge.target][edge.source] = Object.create(null)
            state.graph.allNeighborsIndex[edge.target][edge.source][edge.id] = edge
          }
        }

        // Keep counts up to date:
        state.graph.inNeighborsCount[edge.target]++
        state.graph.outNeighborsCount[edge.source]++
        state.graph.allNeighborsCount[edge.target]++
        state.graph.allNeighborsCount[edge.source]++
      },
      [SET_NODE_PAGE_NUMBER] (state, payload) {
        state.navData.nodePageNumbers.set(payload.key, payload.value)
      },
      [SET_NAV_CURSOR] (state, number) {
        state.navData.navCursor = number
      },
      [PUSH_NAV_HISTORY] (state, payload) {
        state.navData.navHistory = payload
      }
    },
    actions: {
      initGraph (context, payload) {
        context.commit('INSERT_GRAPH', payload)
      },
      setSelectedNode (context, nodeId) {
        context.commit('SET_SELECTED_NODE', nodeId)
        this.dispatch('pushNavHistory', nodeId)
      },
      addNode (context, node) {
        // Check that the node is an object and has an id:
        if (Object(node) !== node || arguments.length < 2) {
          throw Error('addNode: Wrong arguments.')
        }

        if (typeof node.id !== 'string' && typeof node.id !== 'number') {
          throw TypeError('addNode: The node must have a string or number id.')
        }

        if (this.state.graph.nodesIndex[node.id]) {
          throw Error('addNode: The node "' + node.id + '" already exists.')
        }
        // Re-map values according to config values
        let nNode = {}
        if (node.hasOwnProperty('label') && this.state.config.cardLabels.hasOwnProperty(node.label)) {
          let cardLabel = this.state.config.cardLabels[node.label]

          for (let p in cardLabel) {
            if (cardLabel.hasOwnProperty(p)) {
              switch (p) {
                case 'id':
                  nNode.id = node[cardLabel[p]]
                  break
                case 'label':
                  nNode.label = node[cardLabel[p]]
                  break
                case 'name':
                  nNode.name = node[cardLabel[p]]
                  break
                case 'description':
                  nNode.description = node[cardLabel[p]]
                  break
                case 'link':
                  nNode.link = node[cardLabel[p]]
                  break
              }
            }
          }
        } else if (node.hasOwnProperty('label') && this.state.config.tileLabels.hasOwnProperty(node.label)) {
          let tileLabel = this.state.config.tileLabels[node.label]

          for (let p in tileLabel) {
            if (tileLabel.hasOwnProperty(p)) {
              switch (p) {
                case 'id':
                  nNode.id = node[tileLabel[p]]
                  break
                case 'label':
                  nNode.label = node[tileLabel[p]]
                  break
                case 'name':
                  nNode.name = node[tileLabel[p]]
                  break
              }
            }
          }
        } else throw new Error('addNode: Missing config object for card or tile label: ' + JSON.stringify(node))

        context.commit('ADD_NODE', nNode)
      },
      addEdge (context, edge) {
        // Check that the edge is an object and has an id:
        if (Object(edge) !== edge || arguments.length < 2) {
          throw Error('addEdge: Wrong arguments.')
        }

        if (typeof edge.id !== 'string' && typeof edge.id !== 'number') {
          throw TypeError('The edge must have a string or number id.')
        }

        if ((typeof edge.source !== 'string' && typeof edge.source !== 'number') ||
            !this.state.graph.nodesIndex[edge.source]) {
          throw Error('The edge source must have an existing node id.')
        }

        if ((typeof edge.target !== 'string' && typeof edge.target !== 'number') ||
            !this.state.graph.nodesIndex[edge.target]) {
          throw Error('The edge target must have an existing node id.')
        }
        if (this.state.graph.edgesIndex[edge.id]) {
          throw Error('The edge "' + edge.id + '" already exists.')
        }

        context.commit('ADD_EDGE', edge)
      },
      pushNavHistory (context, nodeId) {
        let nodeIdent = nodeId && (typeof nodeId === 'number' || typeof nodeId === 'string')
          ? nodeId : new TypeError('pushNavHistory: payload.nodeId not a number or string.')

        if (nodeIdent instanceof TypeError) { throw nodeId }

        let navHistoryMod = this.state.navData.navHistory
        if (navHistoryMod instanceof Array) {
          // Check if there are trailing history from cursor, if so, slice em out
          if ((navHistoryMod.length - 1) - this.state.navData.navCursor > 0) {
            navHistoryMod = navHistoryMod.slice(0, this.state.navData.navCursor + 1)
          }
          navHistoryMod.push(nodeIdent)
          context.commit('PUSH_NAV_HISTORY', navHistoryMod)
          context.commit('SET_NAV_CURSOR', navHistoryMod.indexOf(nodeIdent))
        }
      }
    },
    getters: {
      getGraph: state => {
        return state.graph
      },
      getNodesByLabel: (state) => (label) => {
        return state.graph.nodesArray.filter(n => n.label === label)
      },
      getChildNodeTiles: (state) => {
        let retval = []
        for (let i in state.graph.inNeighborsIndex[state.selectedNode.id]) {
          let node = state.graph.nodesIndex[i]
          if (node === Object(node) && node.hasOwnProperty('label') &&
                state.config.tileLabels.hasOwnProperty(node.label)) {
            retval.push(node)
          }
        }
        return retval
      },
      /**
       * Getter to retrieve cards from selected node and all child nodes
       * this behaviour is configurable
       */
      getChildNodeCards: (state, getters) => {
        let retval = [],
          tiles = []

        if (state.config.retrieveAllChildNodeCards) {
          for (let x of getters.getChildNodeTiles) {
            tiles.push(x)
          }
        }

        if (state.selectedNode === Object(state.selectedNode) &&
            typeof state.selectedNode.hasOwnProperty === 'function' &&
            state.selectedNode.hasOwnProperty('label') &&
            state.config.tileLabels.hasOwnProperty(state.selectedNode.label)) {
          tiles.push(state.selectedNode)
        }

        for (let i of tiles) {
          let childNodeInIndex = state.graph.nodesIndex[i.id] ? state.graph.inNeighborsIndex[i.id] : {}
          for (let n in childNodeInIndex) {
            let node = state.graph.nodesIndex[n]
            if (node === Object(node) && node.hasOwnProperty('label') &&
                  state.config.cardLabels.hasOwnProperty(node.label)) {
              retval.push(node)
            }
          }
        }
        return retval
      }
      // getFilteredGraph
      // getNodes
      // getEdges
      // getNodeById
      // getEdgeById
    }
  })

  return Store
}
