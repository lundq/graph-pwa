export const graph = {
  nodes: [
    {
      id: '1',
      type: 'node',
      label: 'Event',
      name: 'Båtmässan 2019'
    },
    {
      id: '2',
      type: 'node',
      label: 'Participant',
      name: 'AMT Boats',
      content: `AMT står för Advanced Marine Tech.
      En AMT är mycket noga genomtänkt. Och det märker du direkt i design, sjöegenskaper och kvalitetskänsla. Vi vågar påstå att varje AMT håller högst kvalitet och funktionalitet i sitt segment. AMT är en båt som de allra mest krävande båtägarna rekommenderar.
      AMT-båtarna har välbalanserade, tysta och starka dubbelskrov som ger goda sjöegenskaper även under mycket tuffa förhållanden. Det här är båtar som kombinerar körglädje med en säker gång i alla situationer.`,
      webpage: 'http://www.amtboats.se'
    },
    {
      id: '3',
      type: 'node',
      label: 'Participant',
      name: 'B.Hammar AB',
      content: `Fullservicevarvet för din båt!
      Auktoriserad Volvo Penta, Yamaha & Evinrude verkstad sedan lång tid tillbaka.
      Komplett motorverkstad samt drevverkstad.`,
      webpage: 'http://www.bjornhammarvarvet.se'
    },
    {
      id: '4',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Services'
    },
    {
      id: '5',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Products'
    },
    {
      id: '6',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Repairs'
    },
    {
      id: '7',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Boats'
    },
    {
      id: '8',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Engines'
    },
    {
      id: '9',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Sails'
    },
    {
      id: '10',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Clothing'
    },
    {
      id: '11',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Life vests'
    },
    {
      id: '12',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Apparel'
    },
    {
      id: '13',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Misc'
    },
    {
      id: '14',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Masts'
    },
    {
      id: '15',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Navigaton'
    },
    {
      id: '16',
      type: 'node',
      label: 'ParticipantCategory',
      name: 'Cabin'
    },
    {
      id: '17',
      type: 'node',
      label: 'Participant',
      name: 'Arronet Teknik AB',
      content: `Arronet Aluminiumbåtar - En livslång kärlek i alla väder!
      Svenskbyggda aluminiumbåtar som skräddarsys efter användning - För dig som värdesätter tiden på havet.
      Varmt välkomna att beställa eller känna på dem i sjön här i Göteborg! 
      Ring 031-7890210 eller maila info@svartskarmarin.se`,
      webpage: 'http://www.svartskarmarin.se'
    },
    {
      id: '18',
      type: 'node',
      label: 'Participant',
      name: 'BlueOrbis AB',
      content: `MADE BY THE ELEMENTS - POLARCIRKEL RBB
      Härliga turer på sjön med full fart, på vattenskidor,
      fisketurer eller bara sköna turer med familj och goda vänner,
      ofta i bra väder med blå himmel och lugnt hav - men inte alltid.
      Därför är det smart att satsa på marknadens tuffaste och säkraste fritidsbåt.
      En båt utvecklad för att klara av extrema väderförhållanden. 
      Det har gjort Polarcirkel till en mycket populär båt i Norge,
      såväl bland proffs som bland fritidsbåtägare.
      Bland annat använder Hurtigruten Polarcirkel för guidade turer uppe i Arktis.
      Polarcirkel RBB (Rigid Buoyance Boats) marknadens säkraste och tuffaste båtar.`,
      webpage: 'http://www.svartskarmarin.se'
    }
  ],
  edges: [
    { id: '4:1', source: '4', target: '1', type: 'relation' },
    { id: '5:1', source: '5', target: '1', type: 'relation' },
    { id: '6:4', source: '6', target: '4', type: 'relation' },
    { id: '7:5', source: '7', target: '5', type: 'relation' },
    { id: '8:5', source: '8', target: '5', type: 'relation' },
    { id: '9:5', source: '9', target: '5', type: 'relation' },
    { id: '10:5', source: '10', target: '5', type: 'relation' },
    { id: '11:5', source: '11', target: '5', type: 'relation' },
    { id: '12:5', source: '12', target: '5', type: 'relation' },
    { id: '13:5', source: '13', target: '5', type: 'relation' },
    { id: '14:5', source: '14', target: '5', type: 'relation' },
    { id: '15:5', source: '15', target: '5', type: 'relation' },
    { id: '16:5', source: '16', target: '5', type: 'relation' },
    { id: '2:4', source: '2', target: '4', type: 'predicate', label: 'listedOn' },
    { id: '3:5', source: '3', target: '5', type: 'predicate', label: 'listedOn' },
    { id: '2:7', source: '2', target: '7', type: 'predicate', label: 'listedOn' },
    { id: '3:6', source: '3', target: '6', type: 'predicate', label: 'listedOn' },
    { id: '17:4', source: '17', target: '4', type: 'predicate', label: 'listedOn' },
    { id: '18:5', source: '18', target: '5', type: 'predicate', label: 'listedOn' }
  ]
}
