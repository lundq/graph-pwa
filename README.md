# GRAPH-PWA WITH QUASAR, VUE.JS AND VUEX

This project is to test to browse a graph structure within a mobile progressive web app  
In the state config you could imagine read access to a backend or direct to a neo4j database for example.  
  
Build with *npm b* or *yarn b* and develop with *npm d* or *yarn d*

Please note that this is a project for a *mobile device screen*
  
It would be possible to develop a single page application which could generate a config for the project.  
It could:  
- Retrieve graph schematics to define labels and edge types  
- Define nodes for cards, in and/or out direction, attributes for description etc.  
- Define nodes for tiles, in and/or out direction, attributes for sort/group, edge types  
- Define actions or inputs (bar code scan etc...)  
  
User management  
- Define user input registrations (clicks etc...)  
- Define log in management (work accounts, db accounts)  
  
Domain management  
- Define access domain (https://subdomain.domain.top/secretsubfolder)  
  
Security management  
- Define login authentication like Oauth for major services  
- Browser client encryption to encrypt the graph store with crypto api  

## // Lundq